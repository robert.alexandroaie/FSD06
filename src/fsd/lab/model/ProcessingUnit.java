/**
 * 
 */
package fsd.lab.model;

import java.io.Serializable;

/**
 * @author Robert
 *
 */
public class ProcessingUnit implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;
	private long hardwareClock;
	private long adjustment;
	private long delay;

	public ProcessingUnit(int id) {
		this(id, 0);
	}

	public ProcessingUnit(int id, long delay) {
		this.id = id;
		hardwareClock = System.currentTimeMillis();
		this.delay = delay;
		adjustment = 0;
	}

	/**
	 * @return the id
	 */
	public synchronized int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public synchronized void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the hardwareClock
	 */
	public synchronized long getHardwareClock() {
		return hardwareClock;
	}

	/**
	 * @param hardwareClock
	 *            the hardwareClock to set
	 */
	public synchronized void setHardwareClock(long hardwareClock) {
		this.hardwareClock = hardwareClock;
	}

	/**
	 * @return the adjustment
	 */
	public synchronized long getAdjustment() {
		return adjustment;
	}

	/**
	 * @param adjustment
	 *            the adjustment to set
	 */
	public synchronized void setAdjustment(long adjustment) {
		this.adjustment = adjustment;
	}

	/**
	 * @return the delay
	 */
	public synchronized long getDelay() {
		return delay;
	}

	/**
	 * @param delay
	 *            the delay to set
	 */
	public synchronized void setDelay(long delay) {
		this.delay = delay;
	}
}
