/**
 * 
 */
package fsd.lab.model;

import java.util.ArrayList;
import java.util.List;

import mpi.MPI;

/**
 * @author Robert
 *
 */
public class LundeliusLinch {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		List<ProcessingUnit> cpus = new ArrayList<>();

		MPI.Init(args);

		int rank = MPI.COMM_WORLD.Rank();
		int size = MPI.COMM_WORLD.Size();
		int tag = 0;

		initProcessingUnits(cpus, size);

		allBroadcast(size, tag, cpus);

		long receivedTime=0;
		MPI.COMM_WORLD.Bcast(receivedTime, 0, 8, MPI.LONG, rank);

		MPI.Finalize();
	}

	/**
	 * @param cpus
	 * @param size
	 */
	private static void initProcessingUnits(List<ProcessingUnit> cpus, int size) {
		for (int i = 0; i < size; i++) {
			cpus.add(new ProcessingUnit(i, 1000 * 60 * 60 * i));
		}
	}

	/**
	 * @param size
	 * @param tag
	 * @param currentCPU
	 */
	private static void allBroadcast(int size, int tag, List<ProcessingUnit> cpus) {
		for (int i = 0; i < size; i++) {
			ProcessingUnit pu = cpus.get(i);
			MPI.COMM_WORLD.Isend(pu.getHardwareClock() + getNetworkDelay(), 0, 8, MPI.LONG, pu.getId(), tag);
		}
	}

	/**
	 * @return
	 */
	private static int getNetworkDelay() {
		return 1000 * 60 * 6;
	}

}
